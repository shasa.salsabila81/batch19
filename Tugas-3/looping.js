//soal 1

console.log('=====================Soal 1============================='); 
console.log('LOOPING PERTAMA');
var x = 0;
while(x < 20) { // Loop akan terus berjalan selama nilai kurang dari sama dengan 20
  x +=2; // Menambahkan nilai variable dengan 2
  console.log(x, '- I love coding ')
}

console.log('LOOPING KEDUA');
var y = 20;
while((y>0)&&(y<=20)) { // Loop akan terus berjalan selama nilai di range 0 hingga 20
  console.log(y, '- I will become a mobile developer')
  y -=2; // Mengurangkan nilai variable dengan 2

}

//soal 2
console.log('=====================Soal 2============================='); 
for(var angka = 1; angka <= 20; angka++) {
    if((angka%2 == 1)&&(angka%3 == 0)){
        console.log(angka + ' - I Love Coding');
    }
    else if(angka%2 == 1){
        console.log(angka + ' - Santai');
    }
    else{
        console.log(angka + ' - Berkualitas');
    }
  } 

//soal 3
console.log('=====================Soal 3============================='); 

for(var j = 1; j <=4; j++) {
    var result  = '';
    for(var i = 1; i <= 8; i++) {
        result = result + '#';
      }
      console.log(result)
  } 

  
//soal 4
console.log('=====================Soal 4============================='); 
var result  = '';
for(var j = 1; j <=7; j++) {
    result = result + '#';
    console.log(result)
  } 

  //soal 5
console.log('=====================Soal 5============================='); 

for(var j = 1; j <=8; j++) {
    var result  = '';
    for(var i = 1; i <= 8; i++) {
        if((i+j)%2==1){
            result = result + '#';
        }
        else{
            result = result + ' ';
        }
        
      }
      console.log(result)
  } 