
console.log('=====================Soal 1============================='); 
function arrayToObject(arr) {
    // Code di sini 
    var result = {};
    var now = new Date();
    var thisYear = now.getFullYear(); // 2020 (tahun sekarang)
    for(var i = 0; i < arr.length; i++) {
        if (arr[i][3] < thisYear){
            umur = thisYear - arr[i][3];
        } 
        else{
            umur = "Invalid birth year"
        }
        result = {
            firstName : arr[i][0],
            lastName : arr[i][1],
            gender : arr[i][2],
            age : umur
        }
        console.log((i+1)+'. '+ result.firstName+' '+result.lastName+':')
        console.log(result);
    }
}

var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]

arrayToObject(people)
arrayToObject(people2)
arrayToObject([])



console.log('=====================Soal 2============================='); 
function listpur(money){
    if (money>= 2475000){
        result =[[ 'Sepatu Stacattu','Baju Zoro','Baju H&N','Sweater Uniklooh','Casing Handphone' ],money-2475000]
    }
    else if (money>= 1975000){
        result =[['Baju H&N','Sweater Uniklooh','Casing Handphone','Baju Zoro'],money-1975000]
    }
    else if (money>= 475000){
        result =[['Baju H&N','Sweater Uniklooh','Casing Handphone'],money-475000]
    }
    else if (money>= 225000){
        result =[['Sweater Uniklooh','Casing Handphone'],money-225000]
    }
    else if (money>= 50000){
        result =[['Casing Handphone'],money-50000]
    }
    else{
        result = []
    }
    return result
}


function shoppingTime(memberId="", money) {
    var result = {}
    if (memberId == ""){
        return ("Mohon maaf, toko X hanya berlaku untuk member saja")
    }
    else if (money < 50000){
        return ("Mohon maaf, uang tidak cukup")
    }
    else{
        result = {
            memberId : memberId,
            money: money,
            listPurchased: listpur(money)[0],
            changeMoney: listpur(money)[1]            
        }
    }
    return result;

}
   
  // TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000));
    //{ memberId: '1820RzKrnWn08',
    // money: 2475000,
    // listPurchased:
    //  [ 'Sepatu Stacattu',
    //    'Baju Zoro',
    //    'Baju H&N',
    //    'Sweater Uniklooh',
    //    'Casing Handphone' ],
    // changeMoney: 0 }
  console.log(shoppingTime('82Ku8Ma742', 170000));
  //{ memberId: '82Ku8Ma742',
  // money: 170000,
  // listPurchased:
  //  [ 'Casing Handphone' ],
  // changeMoney: 120000 }
  console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
  console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
  console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja
  
  
  console.log('=====================Soal 3=============================');   
  function uangbayar(awal,akhir) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    bayar = Math.abs(rute.indexOf(akhir) - rute.indexOf(awal)) * 2000 
    return bayar;
  }

  function naikAngkot(arrPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    //your code here
    var result =[];
    for(var i = 0; i < arrPenumpang.length; i++) {
        obj = {
            penumpang: arrPenumpang[i][0],
            naikdari : arrPenumpang[i][1],
            tujuan : arrPenumpang[i][2],
            bayar : uangbayar(arrPenumpang[i][2],arrPenumpang[i][1])
 
        }
        result.push(obj)
    }
    return result;
  }
   
  //TEST CASE
  console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
  // [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
  //   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
   
  console.log(naikAngkot([])); //[]