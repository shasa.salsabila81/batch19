/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Button,
  Text,
  View,
  Image,
  TouchableOpacity,
  FlatList
} from 'react-native';
import { MaterialCommunityIcons } from '@expo/vector-icons';

export default class App extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.header}>
            <Text style={styles.headerText}>Tentang Saya</Text>
        </View>

        <View style={styles.body}>
            
            <View>
            <MaterialCommunityIcons name="account-circle" size={200} color="grey" />
            </View>

            <View style={{flexDirection:"column",justifyContent: 'space-around', alignItems:'center'}}>
                <View>
                    <Text style={{  fontStyle: "normal",  fontWeight: "bold",  fontSize: "24px",  lineHeight: "28px",  color: "#003366"}}>Mukhlis Hanafi</Text>
                </View>

                <View>
                    <Text style={{  fontStyle: "normal",  fontWeight: "bold",  fontSize: "16px",  lineHeight: "19px",  color: "#3EC6FF"}}>React Native Developer</Text>
                </View>

                <View>
                    <Text style={{textAlign: 'left', alignSelf: 'stretch',fontStyle: "normal",   fontWeight: "normal",   fontSize: "18px",   lineHeight: "21px",   color: "#003366"}}>Portfolio</Text>
                    <View style={{  width: "343px",  height: "0px", border: "1px solid #003366"}}></View>

                </View>

                <View style={styles.tabBar}>
                    <View style={styles.tabItem}>
                    <MaterialCommunityIcons name="gitlab" size={24} color="black" />
                        <Text style={styles.tabTitle}>Home</Text>
                    </View>

                    <View style={styles.tabItem}>
                        <MaterialCommunityIcons name="github-circle" size={24} color="black" />
                        <Text style={styles.tabTitle}>Home</Text>
                    </View>

                </View>
            </View>

        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  header: {
    height: 375,
    height: 116,
    backgroundColor: 'white',
    elevation: 3,
    paddingHorizontal: 15,
    alignItems: 'center',
  },
  body: {
    flex: 1,
    flexDirection: "column",
    justifyContent: 'space-around',
    alignItems:'center'
  },

  buttontext: {
    fontStyle: "normal",
    fontWeight: "normal",
    fontSize: "24px",
    lineHeight: "28px",
    color: "#FFFFFF",
    alignSelf:'center',

  },
  
  headerText:{
    fontStyle: "normal",
    fontWeight: "bold",
    fontSize: "36px",
    lineHeight: "42px",
    color: "#003366"
  }
  ,
  box : {
    width: "294px",
    height: "48px",
    background: "#FFFFFF",
    border: "1px solid #003366",
    boxSizing: "border-box"
  },

  tombolmulai : {
    width: "140px",
    height: "40px",
    border: "1px solid #003366",
    boxSizing: "border-box",
    borderRadius: "16px",
    justifyContent:'center',
    backgroundColor: "#003366"
    
  },

  tombolmasuk : {
    width: "140px",
    height: "40px",
    border: "1px solid #3EC6FF",
    boxSizing: "border-box",
    borderRadius: "16px",
    justifyContent:'center',
    backgroundColor: "#3EC6FF"
    
  },
  tabBar: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    width: "359px",
    height: "140px"
  },
  tabItem: {
    alignItems: 'center',
    justifyContent: 'center'
  },

  fieldName: {
    fontStyle: "normal",
    fontWeight: "normal",
    fontSize: "16px",
    lineHeight: "19px",
    color: "#003366"
  },

  field:{ 
      width: "294px", 
      height: "71px",
    },
});

