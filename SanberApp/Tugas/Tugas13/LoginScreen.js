/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Button,
  Text,
  View,
  Image,
  TouchableOpacity,
  FlatList
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

export default class App extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <Image source={require('./images/logo.png')} style={{resizeMode: "center", height: 102, width: 375}}/>
        </View>

        <View style={styles.body}>
            
            <View>
                <Text style={styles.reg}>Registrasi</Text>
            </View>

            <View style={{flexDirection:"column",justifyContent: 'space-around', alignItems:'center'}}>
                <View style={styles.field}>
                    <Text style={styles.fieldName}>Username</Text>
                    <View style={styles.box}></View>
                </View>

                <View>
                    <Text style={styles.fieldName}>Email</Text>
                    <View style={styles.box}></View>
                </View>

                <View>
                    <Text style={styles.fieldName}>Password</Text>
                    <View style={styles.box}></View>
                </View>

                <View>
                    <Text style={styles.fieldName}>Ulangi Password</Text>
                    <View style={styles.box}></View>
                </View>
            </View>

            <View style={styles.tombolmulai}>
                <Text style={styles.buttontext}>Mulai</Text>
            </View>

            <View>
                <Text style={styles.reg}>Atau</Text>
            </View>  

            <View style={styles.tombolmasuk}>
                <Text style={styles.buttontext}>Masuk</Text>
            </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  header: {
    height: 375,
    height: 116,
    backgroundColor: 'white',
    elevation: 3,
    paddingHorizontal: 15,
    alignItems: 'center',
  },
  body: {
    flex: 1,
    flexDirection: "column",
    justifyContent: 'space-around',
    alignItems:'center'
  },

  buttontext: {
    fontStyle: "normal",
    fontWeight: "normal",
    fontSize: "24px",
    lineHeight: "28px",
    color: "#FFFFFF",
    alignSelf:'center',

  },
  
  reg:{
    fontWeight: "normal",
    fontSize: "24px",
    lineHeight: "28px",
    textAlign: 'center',


  },
  box : {
    width: "294px",
    height: "48px",
    background: "#FFFFFF",
    border: "1px solid #003366",
    boxSizing: "border-box"
  },

  tombolmulai : {
    width: "140px",
    height: "40px",
    border: "1px solid #003366",
    boxSizing: "border-box",
    borderRadius: "16px",
    justifyContent:'center',
    backgroundColor: "#003366"
    
  },

  tombolmasuk : {
    width: "140px",
    height: "40px",
    border: "1px solid #3EC6FF",
    boxSizing: "border-box",
    borderRadius: "16px",
    justifyContent:'center',
    backgroundColor: "#3EC6FF"
    
  },

  fieldName: {
    fontStyle: "normal",
    fontWeight: "normal",
    fontSize: "16px",
    lineHeight: "19px",
    color: "#003366"
  },

  field:{ 
      width: "294px", 
      height: "71px",
    },
});

