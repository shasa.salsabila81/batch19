console.log('=====================Soal 1============================='); 

function range(startNum, finishNum){
  result = [];
  if (startNum <= finishNum) {
    for(var i = startNum; i <= finishNum; i++) {
      result.push(i);
    }
  }

  else if (startNum >= finishNum) {
    for(var i = startNum; i >= finishNum; i--) {
      result.push(i);
    }
  }
  
  else{
    result=-1;
  }
  return result;

}
 
console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 

console.log('=====================Soal 2============================='); 
result=[]
function rangeWithStep(startNum, finishNum,step){
  result = [];
  if (startNum <= finishNum) {
    for(var i = startNum; i <= finishNum; i=i+step) {
      result.push(i);
    }
  }

  else if (startNum >= finishNum) {
    for(var i = startNum; i >= finishNum; i=i-step) {
      result.push(i);
    }
  }
  return result;

}


console.log(result);

// Code di sini
 
console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 

console.log('=====================Soal 3============================='); 
result=[]
result=[]
function sum(startNum, finishNum=0,step=1){
  result = 0;
  if (startNum <= finishNum) {
    for(var i = startNum; i <= finishNum; i=i+step) {
      result = result+i;
    }
  }

  else if (startNum >= finishNum) {
    for(var i = startNum; i >= finishNum; i=i-step) {
      result = result+i;
    }
  }
  return result;
}

// Code di sini
console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 

console.log('=====================Soal 4============================='); 

//contoh input
var input = [
  ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
  ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
  ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
  ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
] 

function dataHandling (matriks){
  for(var i = 0; i < matriks.length; i++) {
    var array = matriks[i];
    console.log("Nomor ID: "+array[0])
    console.log("Nama Lengkap: "+array[1])
    console.log("TTL: "+array[2] +' '+ array[3])
    console.log("Hobi: "+array[4]+"\n");
  }
  
}

dataHandling(input)

console.log('=====================Soal 5============================='); 

function balikKata(kata){
  result = [];
  for (var i=kata.length-1; i>=0; i--){
    result = result + (kata[i]);
  }
  return result;
}

console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 

console.log('=====================Soal 6============================='); 

input2 = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"];  

function splitbulan(data){
  tglsplit = data[3].split("/");
  return tglsplit;
}

function bulanlahir(data){
  tglsplit = splitbulan(data);
  switch(tglsplit[1]) {
    case '01':   { tglsplit[1] ='Januari'; break; }
    case '02':   { tglsplit[1] ='Februari'; break; }
    case '03':   { tglsplit[1] ='Maret'; break; }
    case '04':   { tglsplit[1] ='April'; break; }
    case '05':   { tglsplit[1] ='Mei'; break; }
    case '06':   { tglsplit[1] ='Juni'; break; }
    case '07':   { tglsplit[1] ='Juli'; break; }
    case '08':   { tglsplit[1] ='Agustus'; break; }
    case '09':   { tglsplit[1] ='September'; break; }
    case '10':   { tglsplit[1] ='Oktober'; break; }
    case '11':   { tglsplit[1] ='November'; break; }
    case '12':   { tglsplit[1] ='Desember'; break; }}
  return tglsplit[1]; 
}

function bulanlahirsort(data){
  tglsplit = splitbulan(data);
  return tglsplit.sort(function(a, b){return b-a});
}  

function dataHandling2 (array){
  array[1] = array[1]+ " Elsharawy";
  array[2] = "Provinsi " + array[2];
  array.splice(4, 1, "Pria");
  array.splice(5, 0, "SMA Internasional Metro");
  console.log(array);
  console.log(bulanlahir(array))
  console.log(bulanlahirsort(array))
  console.log(splitbulan(array).join('-'))
  console.log(array[1].slice(0,15))

}
  

dataHandling2(input2)