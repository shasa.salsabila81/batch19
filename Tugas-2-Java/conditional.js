//If else
var nama = "John"
var peran = ""

console.log('-------------------------------Soal if else------------------------------------')
if ( nama != "" ) {
    if(peran == "" ) {
        console.log('Halo '+ nama + ', Pilih peranmu untuk memulai game!')    
    } else if( peran == "Penyihir") {
        console.log("Selamat datang di Dunia Werewolf, " + nama)
        console.log("Halo "+ peran+ " Jane, kamu dapat melihat siapa yang menjadi werewolf!")
    } else if ( peran == "Guard" ) {
        console.log("Selamat datang di Dunia Werewolf, " + nama)
        console.log("Halo Guard "+ nama+ ", kamu akan membantu melindungi temanmu dari serangan werewolf.")
    } else if ( peran == "Warewolf" ) {
        console.log("Selamat datang di Dunia Werewolf, " + nama)
        console.log("Halo Warewolf "+ nama+ ", Kamu akan memakan mangsa setiap malam!")
    }
} else {
    console.log("Nama harus diisi!")
}

//Switcase
var hari = 21; 
var bulan = 1; 
var tahun = 1945;
//  Maka hasil yang akan tampil di console adalah: '21 Januari 1945'; 

var tanggal = hari; // assign nilai variabel tanggal disini! (dengan angka antara 1 - 31)

console.log('-------------------------------Soal Switch Case------------------------------------')
switch(bulan) {
case 1:   { console.log(tanggal+' Januari '+tahun); break; }
case 2:   { console.log(tanggal+' Februari '+tahun); break; }
case 3:   { console.log(tanggal+' Maret '+tahun); break; }
case 4:   { console.log(tanggal+' April '+tahun); break; }
case 5:   { console.log(tanggal+' Mei '+tahun); break; }
case 6:   { console.log(tanggal+' Juni '+tahun); break; }
case 7:   { console.log(tanggal+' Juli '+tahun); break; }
case 8:   { console.log(tanggal+' Agustus '+tahun); break; }
case 9:   { console.log(tanggal+' September '+tahun); break; }
case 10:   { console.log(tanggal+' Oktober '+tahun); break; }
case 11:   { console.log(tanggal+' November '+tahun); break; }
case 12:   { console.log(tanggal+' Desember '+tahun); break; }}